var debug = require('debug')('untitled:server');

module.exports = function(app) {
    var port = '443';
    var https = require('https');
    app.set('port', port);

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        var bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    var tls = require('tls');
    var fs = require('fs');

    var options = {
        key  : fs.readFileSync('/home/4buy_parse/private.key'),
        cert : fs.readFileSync('/home/4buy_parse/cert.cert')
    };

    var server = https.createServer(options, app);
    function onListening() {
        var addr = server.address();
        var bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
        debug('Listening on ' + bind);
    }

    server.on('error', onError);
    server.on('listening', onListening);
    server.listen(443, function () {
        console.log('Started!');
    });
   // server.listen(port);
};


