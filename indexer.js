#!/usr/bin/env node

l = console.log.bind();
currentUser = {};

var
    underscore = require('underscore'),
    minify = require('html-minifier').minify,
    moment = require('moment'),
    Parse = require('parse/node'),
    uglifycss = require('uglifycss'),
    purify = require('purify-css');

Parse.initialize("123", 'myJsKey', 'mySecretMasterKey');
Parse.serverURL = 'https://api.4buy.com.ua/parse';
Parse.Cloud.useMasterKey();


var pagesQueue = [{
    url: 'https://4buy.com.ua/',
    prio: 1
}];


var categories = new Parse.Object.extend("categories");
var staticObj = new Parse.Object.extend("staticPages");
var shopItems = new Parse.Object.extend("shopItems");
var blogItems = new Parse.Object.extend("blogItems");
var cachedPage = new Parse.Object.extend("cachedPages");
var catQuery = new Parse.Query(categories);
var staticQuery = new Parse.Query(staticObj);
var shopItemsQuery = new Parse.Query(shopItems);
var blogItemsQuery = new Parse.Query(blogItems);
var rootUrl = 'https://4buy.com.ua/';


shopItemsQuery.limit(2000);

staticQuery.find({
    success: function (staticPages) {
        blogItemsQuery.find({
            success: function (blogItems) {
                shopItemsQuery.find({
                    success: function (shopItems) {
                        catQuery.find({
                            success: function (cats) {
                                underscore.each(staticPages, function(staticPage){
                                    pagesQueue.push({
                                        url: rootUrl + staticPage.get('url') + '/',
                                        prio: 0.7
                                    })
                                });
                                underscore.each(cats, function (cat) {
                                    if (cat.get('catType') == 'shop') {
                                        if (cat.get('parentId') == '') {
                                             pagesQueue.push({
                                                 url: rootUrl + cat.get('url') + '/',
                                                 prio: 0.9
                                             });
                                            underscore.each(shopItems, function (shopItem) {
                                                if (shopItem.get('catId') == cat.id) {
                                                     pagesQueue.push({
                                                         url: rootUrl + cat.get('url') + '/' + shopItem.get('url'),
                                                         prio: 0.8
                                                     });
                                                }
                                            });
                                        } else {
                                            underscore.each(cats, function (parentCat) {
                                                if (cat.get('parentId') == parentCat.id) {
                                                     pagesQueue.push({
                                                         url: rootUrl + parentCat.get('url') + '/' + cat.get('url') + '/',
                                                         catId: cat.get('parentId'),
                                                         prio: 0.9
                                                     });

                                                    underscore.each(shopItems, function (shopItem) {
                                                        if (shopItem.get('catId') == cat.id) {
                                                             pagesQueue.push({
                                                                 url: rootUrl + parentCat.get('url') + '/' + cat.get('url') + '/' + shopItem.get('url'),
                                                                 prio: 0.8
                                                             });
                                                        }
                                                    });
                                                }
                                            })
                                        }
                                    } else {
                                        if (cat.get('parentId') == '') {
                                             pagesQueue.push({
                                                 url: rootUrl + 'blog/' + cat.get('url') + '/',
                                                 prio: 0.9
                                             });
                                            underscore.each(blogItems, function (blogItem) {
                                                if (blogItem.get('catId') == cat.id) {
                                                     pagesQueue.push({
                                                         url: rootUrl + 'blog/' + cat.get('url') + '/' + blogItem.get('url'),
                                                         prio: 0.8
                                                     });
                                                }
                                            });
                                        } else {
                                            underscore.each(cats, function (parentCat) {
                                                if (cat.get('parentId') == parentCat.id) {
                                                     pagesQueue.push({
                                                         url: rootUrl + 'blog/' + parentCat.get('url') + '/' + cat.get('url') + '/',
                                                         catId: cat.get('parentId'),
                                                         prio: 0.9
                                                     });

                                                    underscore.each(blogItems, function (blogItem) {
                                                        if (blogItem.get('catId') == cat.id) {
                                                             pagesQueue.push({
                                                                 url: rootUrl + 'blog/' + parentCat.get('url') + '/' + cat.get('url') + '/' + blogItem.get('url'),
                                                                 prio: 0.8
                                                             });
                                                        }
                                                    });
                                                }
                                            })
                                        }
                                    }
                                });
                                //List is full
                                generateSitemap();
                                PhantomIndexer();

                            }
                        });
                    }
                });
            }
        });
    }
});
var stored = 0;
var PhantomIndexer = function () {
    var storeLength = pagesQueue.length;
    if (stored <= storeLength) {
        respond(pagesQueue[stored]['url']);
    } else {
        process.exit(0);
    }
};

var getContent = function (url, callback) {
    var content = '';
    var phantomBinary = (process.platform == 'darwin') ? 'phantomjs-osx/bin/phantomjs' : 'phantomjs-linux/bin/phantomjs'
    var phantom = require('child_process').spawn(phantomBinary, ['phantom-server.js', url]);
    phantom.stdout.setEncoding('utf8');
    phantom.stdout.on('data', function (data) {
        content += data.toString();
    });
    phantom.on('exit', function (code) {
        if (code !== 0) {
            callback({
                success: false,
                content: code
            });
        } else {
            callback({
                success: true,
                content: content
            });
        }
    });
};

var respond = function (url) {
    getContent(url, function (phantomResult) {
        if (phantomResult.success) {
            var cacheToDb = new cachedPage();
            var minifed = minify(phantomResult.content, {
                removeComments: false,
                collapseBooleanAttributes: true,
                collapseWhitespace: true,
                removeEmptyAttributes: true
            });

            var cacheName = minifed.split('//cachename//')[1];
            var request = require('request').defaults({ encoding: null });
            request.get('https://4buy.com.ua/cache/'+cacheName+'.css', {
                minify: true
            }, function (err, response, body) {
                purify(minifed, body.toString('utf8') ,function(purifiedCSS){
                    var uglified = uglifycss.processString(
                        purifiedCSS,
                        { expandVars: true }
                    );
                    minifed = minifed.replace('<!--placemincsshere-->', '<style>'+uglified+'</style>');
                    cacheToDb.set("url", url);
                    cacheToDb.set("content", minifed);
                    cacheToDb.set("_MasterKey", 'mySecretMasterKey');
                    cacheToDb.save(null, {
                        success: function (gameScore) {
                            console.log('cache stored', url);
                            stored++;
                            PhantomIndexer();
                        }, error: function (e) {
                            stored++;
                            PhantomIndexer();
                            console.log(e, arguments);
                        }
                    });
                });
            });




        } else {
            stored++;
            PhantomIndexer();
        }
    });
};

var generateSitemap = function () {
    var tpl = '<?xml version="1.0" encoding="UTF-8"?>\n' +
        '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n';
    underscore.each(pagesQueue, function (page) {
        var date = new Date();
        tpl += '<url>\n' +
            '<loc>'+page.url+'</loc>\n' +
            '<lastmod>'+moment().format('YYYY-MM-DD')+'</lastmod>\n' +
            '<changefreq>daily</changefreq>\n' +
            '<priority>'+page.prio+'</priority>\n' +
            '</url>\n'
    });
    tpl += '</urlset>\n';
    var cacheToDb = new cachedPage();
    cacheToDb.set("url", 'https://4buy.com.ua/sitemap.xml');
    cacheToDb.set("content", tpl);
    cacheToDb.save(null, {
        success: function (gameScore) {
            console.log('sitemap stored');
        }, error: function (e) {
            console.log(e, arguments);
        }
    });
};
l('~~~ready~~~');