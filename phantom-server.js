var page = require('webpage').create();
var system = require('system');
var lastReceived = new Date().getTime();
var requestCount = 0;
var responseCount = 0;
var requestIds = [];
var startTime = new Date().getTime();
page.customHeaders = {
    "X-Phantom-bot": "true"
};


page.onResourceReceived = function (response) {
    if(requestIds.indexOf(response.id) !== -1) {
        lastReceived = new Date().getTime();
        responseCount++;
        requestIds[requestIds.indexOf(response.id)] = null;
    }
};
page.onResourceRequested = function (request) {
    if(requestIds.indexOf(request.id) === -1) {
        requestIds.push(request.id);
        requestCount++;
    }
};

// Open the page
page.open(system.args[1], function () {});
//page.open('http://localhost/blog', function () {});

var checkComplete = function () {
    // We don't allow it to take longer than 5 seconds but
    // don't return until all requests are finished
    if((new Date().getTime() - lastReceived > 300 && requestCount === responseCount) || new Date().getTime() - startTime > 10000)  {
        //if(new Date().getTime() - startTime > 10000)  {
        clearInterval(checkCompleteInterval);
        console.log(page);
        phantom.exit();
    }
}

setTimeout(function(){
    console.log(page.content);
    phantom.exit();
},10000);
//
// page.onCallback = function(data){
//     console.log('ok', data);
// };
// Let us check to see if the page is finished rendering
//var checkCompleteInterval = setInterval(checkComplete, 1);