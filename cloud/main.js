Parse.Cloud.beforeSave("orders", function(request, response) {
    var currentDate = new Date();
    query = new Parse.Query("orders");
    query.lessThanOrEqualTo("createdAt", currentDate);
    query.descending("createdAt");
    query.first({
        success: function(result) {
            request.object.set("order_id", (result.get("order_id") + 1));
            response.success();
        },
        error: function() {
            response.success();
            response.error("error");
        }
    });
});