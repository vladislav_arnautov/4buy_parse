#!/usr/bin/env node

l = console.log.bind();
currentUser = {};

var
    express = require('express'),
    path = require('path'),
    app = express(),
    ParseServer = require('parse-server').ParseServer,
    compression = require('compression'),
    OneSignalPushAdapter = require('parse-server-onesignal-push-adapter'),
    oneSignalPushAdapter = new OneSignalPushAdapter({
        oneSignalAppId:"bb78e1a5-18b8-430e-9be6-83390873469d",
        oneSignalApiKey:"M2MwYmY4OTEtYTk4YS00MDEzLTljMmMtNGY2YjdhYTc3MDkz"
    }),
    apiParse = new ParseServer({
        databaseURI: 'mongodb://127.0.0.1:27017/4buy',
        cloud: './cloud/main.js',
        appId: '123',
        // fileKey: '1234',
        masterKey: 'mySecretMasterKey',
        javascriptKey: '',
        clientKey: '',
        serverURL: 'https://sprtmn.com/parse',
        verifyUserEmails: true,
        emailVerifyTokenValidityDuration: 60 * 60 * 24 * 365, // in seconds (2 hours = 7200 seconds)
        preventLoginWithUnverifiedEmail: false,
        publicServerURL: 'https://sprtmn.com/parse',
        appName: 'sprtmn.com',
        emailAdapter: {
            module: 'parse-server-mailgun-adapter-template',
            options: {
                fromAddress: 'no-reply@4buy.com.ua',
                domain: '4buy.com.ua',
                apiKey: 'key-c99bedb0bda2a5e8fa01a0c85bb2804c',
                verificationSubject: 'Подтвердите ваш e-mail для %appname%',
                verificationBody: 'Чтобы завершить <b>регистрацию</b>, необходимо сначала подтвердить Ваш адрес электронной почты . %email% для %appname%\n\nНажмите сюда для подтверждения:\n%link%',
                passwordResetSubject: 'Сброс пароля для %appname%',
                passwordResetBody: 'Изменение пароля вашей учетной записи на %appname%.\n\nНажмите сюда:\n%link%'
            }
        }
    });


var parseCacheControl = require('parse-cache-control');
var Cacher = require("cacher")
var cacher = new Cacher();

cacher.genCacheTtl = function(res, origTtl) {
    if (res.get('Cache-Control')) {
        var cacheControl = parseCacheControl(res.get('Cache-Control'));
        if (cacheControl['s-maxage']) {
            return parseInt(cacheControl['s-maxage']);
        }
        if (cacheControl['max-age']) {
            return parseInt(cacheControl['max-age']);
        }
    }
    if (res.get('Expires')) {
        return (Date.parse(res.get('Expires')) - Date.now()) / 1000;
    }

    return origTtl;
};
app.use(cacher.cache('seconds', 0));

app.use(compression({
    level: 9
}));

app.use('/parse', apiParse);

require('./search')(app);

app.use(express.static(path.join(__dirname, 'public')));


require('./lib/server')(app);

l('~~~ready~~~');